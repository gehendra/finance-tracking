class Stock < ActiveRecord::Base
  attr_accessible :last_price, :name, :ticker

  has_many :user_stocks
  has_many :user, through: :user_stocks

  def self.find_by_ticker(ticker_symbol)
    where(:ticker => ticker_symbol).first
  end

  def self.new_from_lookup(ticker_symbol)
    looked_up_stock = StockQuote::Stock.quote(ticker_symbol)
    return nil unless looked_up_stock.name
    new_stock = new(ticker: looked_up_stock.symbol, name: looked_up_stock.name)
    new_stock.last_price = new_stock.price
    new_stock.save
    return new_stock
  end

  def price
    closing_price = StockQuote::Stock.quote(ticker).close
    return "#{closing_price} (Closing)" if closing_price

    opening_price = StockQuote::Stock.quote(ticker).open
    return "#{opening_price} (Opening)" if opening_price

    'Unavilable'
  end

  def self.update_all
    stock = Stock.all
    stock.each do |s|
      update_stock_price = StockQuote::Stock.quote(s.ticker)
      s.last_price = s.price
      s.save
    end
  end

end
