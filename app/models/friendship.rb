class Friendship < ActiveRecord::Base
  # attr_accessible :title, :body
  belongs_to :user
  belongs_to :friend, class_name: 'User'

  attr_accessible :user_id, :friend_id
end
