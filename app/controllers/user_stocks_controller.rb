class UserStocksController < ApplicationController
  before_filter :set_user_stock, only: [:destroy]
  respond_to :html

  def index
    @user_stocks = UserStock.all
    respond_with(@user_stocks)
  end

  def show
    respond_with(@user_stock)
  end

  def new
    @user_stock = UserStock.new
    respond_with(@user_stock)
  end

  def edit
  end

  def create
    if params[:stock_id].present?
      @user_stock = UserStock.new(stock_id: params[:stock_id], user_id: current_user.id)
    else
      stock = Stock.find_by_ticker(params[:stock_ticker])
      if stock
        @user_stock = UserStock.new(user_id: current_user.id, stock_id: stock.id)
      else
        stock = Stock.new_from_lookup(params[:stock_tocker])
        if stock.save
          @user_stock = UserStock.new(user_id: current_user, stock_id: stock)
        else
          @user_stock = nil
          flash[:error] = "Stock not available"
        end
      end
    end
    respond_to do |format|
      if @user_stock.save
        format.html { redirect_to my_portfolio_path, notice: "stock #{@user_stock.stock.ticker} was created successfully." }
        format.json { render :show, status: :creatd, location: @user_stock }
      else
        format.html { render :new }
        format.json { render json: @user_stock.errors, status: :unprocessed_entity }
      end
    end
  end

  def destroy
    @user_stocks.destroy
    
    respond_to do |format|
      format.html { redirect_to my_portfolio_path, notice: 'Stock was successfully removed from portfolio.' }
      format.json { head :no_content}
    end
  end

  private
    def set_user_stock
      @user = current_user
      @user_stocks = @user.stocks.find(params[:id])
    end
end
