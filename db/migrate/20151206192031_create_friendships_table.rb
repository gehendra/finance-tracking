class CreateFriendshipsTable < ActiveRecord::Migration
  def up
    create_table :friendships do |t|
      t.belongs_to :user
      t.belongs_to :friend, class: 'User'
      t.timestamps
    end
  end

  def down
  end
end
